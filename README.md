# Jeu de la vie - SR01

L’objectif de cet exercice est d’implémenter le jeu de vie vu en TD en utilisant Tkinter
pour réaliser l’interface graphique du jeu. Vous pouvez vous appuyer sur la partie déjà
développée en console pour rajouter après les fonctions permettant de dessiner
l’interface graphique et manipuler le jeu.
L’interface graphique du jeu devra correspondre au schéma suivant :

