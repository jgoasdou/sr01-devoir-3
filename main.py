from functools import partial
import tkinter as tk
from tkinter import ttk
from command import *
from config import config
import random
import time
from ResizingCanvas import ResizingCanvas

g = globals()

if verif_fichier("./config.ini") == FALSE :
    raise Exception('Il manque le fichier config.ini')

parameters = config()
defbg = parameters['background']
deffont = parameters['font']
deffg = parameters['foreground']
deftitle = parameters['title']
defminx = parameters['minsizex']
defminy = parameters['minsizey']


root = Tk() # Création de la fenêtre racine
root.title(deftitle)
root.geometry("800x600")
root.minsize(defminx,defminy)
# root.iconbitmap("chemin")
root.config(background=defbg)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ##  Inputs à récupérer à partir de l'interface ## #
input_taille = DoubleVar()
input_chance = DoubleVar()
input_vitesse = DoubleVar() # Il y a un calcul à faire mais je sais pas encore lequel
tab = create_tab(100) 
canvasTab = create_tab(100)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


MenuOption = Frame(root)
MenuOption.config(width=200)
MenuOption.pack(fill=Y, side= RIGHT)

canvas = ResizingCanvas(root)


#BOTTOM
butQuitter = Button(MenuOption, text="Quitter",width=20, command=root.quit).pack(side=BOTTOM)
Label(MenuOption, text="").pack(side=BOTTOM) #saut de ligne
slider_Vitesse = Scale(MenuOption, width=20, from_=1, to=100,orient=HORIZONTAL, variable=input_vitesse).pack(side=BOTTOM)
Label(MenuOption, text="Vitesse",foreground="blue").pack(side=BOTTOM)
Label(MenuOption, text="").pack(side=BOTTOM) #saut de ligne
slider_Vie = Scale(MenuOption, width=20,from_=0, to=100,orient=HORIZONTAL, variable=input_chance).pack(side=BOTTOM)
Label(MenuOption, text="% de vie",foreground="blue").pack(side=BOTTOM)
Label(MenuOption, text="").pack(side=BOTTOM) #saut de ligne
slider_Taille = Scale(MenuOption, width=20, from_=5, to=100,orient=HORIZONTAL, variable=input_taille).pack(side=BOTTOM)
Label(MenuOption, text="Taille de la grille",foreground="blue").pack(side=BOTTOM)

#TOP
butCommencer = Button(MenuOption, width=20, text="Lancer",foreground="blue", command=lambda: run(int(input_vitesse.get()), int(input_taille.get()), canvas)).pack()
butArreter = Button(MenuOption, width=20, text="Arreter",foreground="blue", command=lambda: pause_game(canvas)).pack()
butInitialiser = Button(MenuOption, width=20, text="Initialiser",foreground="blue", command=lambda: init(int(input_taille.get()), int(input_chance.get()), canvas)).pack()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ##  Suite de commande à lancer lors de l'appuie sur "Initialiser" ## #
# tab = create_tab(input_taille) # Création d'un tableau de taille 10
##debug_print(tab) # Vue du tableau initialisé à 0
# fill_tab(tab,input_chance)
# debug_print(tab) # Vue du tableau initialisé en fonction de la chance de vie à la création
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ##  Commande à lancer lors de l'appuie sur "Lancer" ## #
# tab = run_game(tab,input_vitesse)
##debug_print(tab) # Vue du tableau après l'éxecution
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


canvas.pack(side=LEFT, fill=BOTH, expand=True)

root.mainloop() # Lancement de la boucle principale
