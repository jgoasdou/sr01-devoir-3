from configparser import * 
 
def config(filename='config.ini', section='parameters'):

    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)

    # get section, par défaut à postgresql
    parameters = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            parameters[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return parameters

