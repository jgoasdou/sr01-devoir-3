from os import waitpid
from config import config
from functools import partial, singledispatchmethod
from tkinter import *
from configparser import * 
from pathlib import Path
import subprocess
import random
import time

debug = False

initialized = False
running = False

g = globals()

parameters = config()
defbg = parameters['background']
deffont = parameters['font']
deffg = parameters['foreground']
deftitle = parameters['title']
defminx = parameters['minsizex']
defminy = parameters['minsizey']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ##    Functions dédiées au jeu de la vie   ## #
def create_tab(taille):
    global tab, canvasTab
    tab = [[0 for _ in range(taille)] for _ in range(taille)]
    canvasTab = [[0 for _ in range(taille)] for _ in range(taille)]
    debug_print(tab)
    return tab

def fill_tab(chance):
    global tab
    for i in range(len(tab)):
        for j in range(len(tab)):
            nbr = random.randint(0, 99)
            if nbr < chance :
                tab[i][j] = 1
    debug_print(tab)

def init(taille, chance, canvas):
    global initialized, running
    create_tab(taille)
    fill_tab( chance)
    create_canvas_grid(canvas, taille)
    initialized = True
    running = False

def get_status(posy, posx):
    if tab[posy][posx] == 1 :
        return 1
    return 0

def get_neighbor(posy, posx):
    result = 0
    xrange = [-1, 0, 1]
    yrange = [-1, 0, 1]

    if posy == 0 :
        yrange = [len(tab)-1 ,0, 1]
    if posy == len(tab)-1 :
        yrange = [-1, 0, -(len(tab)-1)]
    if posx == 0 :
        xrange = [len(tab)-1 ,0, 1]
    if posx == len(tab)-1 :
        xrange = [-1, 0, -(len(tab)-1)]

    for deltay in yrange:
            for deltax in xrange:
                result = result + get_status( posy + deltay, posx + deltax)
    
    return result - tab[posy][posx]

def cycle(taille):
    global tab
    newtab = [[0 for _ in range(taille)] for _ in range(taille)]
    for i in range(taille):
        for j in range(taille):
            nbr_voisin = get_neighbor(i, j)
            if (get_status(i, j) and (nbr_voisin == 2 or nbr_voisin == 3)) or (not(get_status(i, j)) and get_neighbor(i, j) == 3) :
                newtab[i][j] = 1
    debug_print(newtab)
    return newtab

def is_alive(tab): #Retourne 1 si au moins une cellule est en vie, 0 sinon
    for i in range(len(tab)):
        for j in range(len(tab)):
            if tab[i][j] == 1 : return 1
    return 0

def number_alive(tab): #Retourne le nombre de cellules en vie
    count = 0
    for i in range(len(tab)):
        for j in range(len(tab)):
            count = tab[i][j] + count
    return count

def run(sleeptime, taille, canvas):
    global running
    running = True
    run_game(sleeptime, taille, canvas)

def run_game(sleeptime, taille, canvas):
    global tab, initialized
    if running == False or initialized == False:
        return
    # debug_print(tab)
    pause = int((1/100)*(101 - sleeptime) * 1000)
    if is_alive(tab):
        tab = cycle(taille)
        debug_print(tab)
        update_canvas_grid(canvas, taille)
        canvas.after(pause, lambda: run_game(sleeptime, taille, canvas))
    return tab

def pause_game(canvas):
    global running
    if running == False:
        return
    running = False
    #canvas.wait_variable(input_wait)

def debug_print(tab):
    global debug
    if(debug == True):
        print('DEBUG PRINT :')
        for i in range(len(tab)):
            print(tab[i])


def create_canvas_grid(canvas, input_taille):
    global tab
    cell_width = canvas.winfo_reqwidth() / input_taille
    cell_height = canvas.winfo_reqheight() /  input_taille
    for column in range(input_taille):
        for row in range(input_taille):
            x1 = column*cell_width
            y1 = row * cell_height
            x2 = x1 + cell_width
            y2 = y1 + cell_height
            #print(f"x1 : {x1}, y1 : {y1}, x2 : {x2}, y2 : {y2}\n")
            if(tab[column][row] == 0):
                canvasTab[column][row] = canvas.create_rectangle(x1,y1,x2,y2, fill="white", outline="black")
            elif(tab[column][row] == 1):
                canvasTab[column][row] = canvas.create_rectangle(x1,y1,x2,y2, fill="red", outline="black")

def update_canvas_grid(canvas, taille):
    global tab, canvasTab
    for column in range(taille):
        for row in range(taille):
            if(tab[column][row] == 0):
                canvas.itemconfig(canvasTab[column][row], fill="white")
            elif(tab[column][row] == 1):
                canvas.itemconfig(canvasTab[column][row], fill="red")


# ##    Fin de la section dédiée au jeu de la vie   ## #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def update_label(label, command, param=None):
    if command == 'text':
        label.config(text = param)
    if command == 'background':
        label.config(background = param)
    if command == 'padx':
        label.config(padx = param)
    if command == 'pady':
        label.config(pady = param)

def verif_fichier(filename):
    fileName = r"{0}".format(filename)
    fileObj = Path(fileName)
    return fileObj.is_file()

def getusersections(filename='user.ini'):

    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)
    user = {}
    if parser.sections():
        user = parser.sections()
    else:
        # print("Aucune catégorie n'est crée") 
        print('')
    return user

def getsectioninfos(section ,filename='user.ini'):

    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)

    # get section, par défaut à postgresql
    info = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            info[param[0]] = param[1]
    else:
        # print("Aucune catégorie n'est crée")
        print('')
    return info

# def getappsection(app)

def getpath(section, app, filename='user.ini'):
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    return parser.get(section, app)

def verify_section(section, filename='user.ini'):
    parser = ConfigParser()
    parser.read(filename)
    return parser.has_section(section)

def verify_app(section, app, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas")
        return FALSE
    parser = ConfigParser()
    parser.read(filename)
    return parser.has_option(section, app)




def addusersection(section, filename='user.ini'):
    if verify_section(section) == TRUE:
        print('la section existe deja')
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)

    parser.add_section(section)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def adduserapp(section, app, path, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_app(section, app) == TRUE:
        print("L'application existe deja!")
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    
    parser.set(section, app, path)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def removeapp(section, app, filename='user.ini'):
    if verify_app(section, app) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    parser.remove_option(section, app)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def removesection(section, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    parser.remove_section(section)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def modifypath(section, app, path, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_app(section, app) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    removeapp(section, app)
    adduserapp(section, app, path)

def modifyapp(section, app, newapp, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_app(section, app) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    if verify_app(section, newapp) == TRUE:
        print("Le nom est deja utilisé!")
        return FALSE

    path = getpath(section, app)
   
    removeapp(section, app)
    adduserapp(section, newapp, path)

def modifysection(section, newsection):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_section(newsection) == TRUE:
        print("Le nom de la nouvelle section est deja prit !")
        return FALSE
    addusersection(newsection)
    sectionval = getsectioninfos(section)  
    for values in sectionval:
        adduserapp(newsection, values, sectionval[values])
    removesection(section)

def openapp(path):
    subprocess.run(path)

